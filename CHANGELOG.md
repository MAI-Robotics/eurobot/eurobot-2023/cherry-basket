# Changelog of cherry basket

## Week 19
- First wood panels cut out, glued together
    ![Wood panels](images/2023-02-19/2023-02-19%20Wood%20panels.jpeg)
- New idea of using aluminium profiles and 3d-printed holders for remaining plates
- fully designed all 3d models
- tested all parts

## Week 20
- Designed Holders for:
    - load cell
    - slant
    - display
- cut plate for load cell
- cut aluminium profiles and finished plan

## Week 21-22
- 3d-printed all holders and arms
- put extra piece of wood on top to secure balls from rolling off
- add holders to basket, add slanted
- add arms, cut screws and alu-profiles
- add middle-aluminium-profile
- add load-cell-mount and loadcell
- Fixed count and positioning of balls:


| ![Fix1](images/2023-03-02/IMG_3009.JPG) | ![Fix2](images/2023-03-02/IMG_3010.JPG) | ![Fix3](images/2023-03-02/IMG_3011.JPG) | ![Fix4](images/2023-03-02/IMG_3013.JPG) |
| --- | --- | --- | --- |

Code:
- Wrote first Code: Count balls using weight, testing display, wrote display library, wrote code to display ballcount on display
- Add debug code to fix counting balls, there are few errors when ballcount gets over the count of 20

## Week 23
- Add bottom plate
- Add display mount
- Update 3d model
- Wiring everything, finished calibration
- first tests in arena