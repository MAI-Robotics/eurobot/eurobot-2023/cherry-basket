# Cherry basket
This is our repo for our code of the cherry basket.
Our 3D-Model can be found [here](https://a360.co/3EmQQaK).

## Parts used
- Wood
- 3D-printed parts
- Arduino Mega
- Adafruit 1.77" TFT Display
- HX711 + Load cell

## Wiring
- [Wiring Diagram](https://1drv.ms/b/s!AjWxvMBValH5g6VY0fiGOBPy2SdMdw?e=1rfPm5)

## Pictures
| ![3D-Render](images/2023-02-23/2023-02-23%201.png) | ![3D-Render 2](images/2023-02-23/2023-02-23%202.png) |
| --- | --- |

| ![Img1](images/2023-02-28/IMG_2978.JPG) | ![Img2](images/2023-02-28/IMG_2979.JPG) |
| --- | --- |
| ![Img3](images/2023-02-28/IMG_2980.JPG) | ![Img4](images/2023-02-28/IMG_2982.JPG) |


## Libraries used
```cpp
#include <Adafruit_GFX.h>    
#include <Adafruit_ST7735.h> 
#include <SPI.h>
#include <Arduino.h>
#include "HX711.h"
```