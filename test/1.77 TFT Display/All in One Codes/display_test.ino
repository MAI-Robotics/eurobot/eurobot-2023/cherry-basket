/***
 * Test für das 1,77 Zoll SPI TFT-Display
 * v1.0 - 2018-01-06
 * Jan Greth <github@greth.me>
 ***/

/* Pinnung
 *  Display (Pin) - Arduino Nano
 *  GND  (1) - GND  
 *  VCC  (2) - 5V
 *  SCK  (3) - D13
 *  SDA  (4) - D11
 *  RES  (5) - D8
 *  RS   (6) - D9
 *  CS   (7) - D10
 *  LEDA (8) - 3.3V
 *  
 *  LEDA kann auch an 5V doch dann wird das Display sehr schnell sehr heiß - was ich nicht für optimal halte.
 *  Beim Betrieb mit 3.3V ist das Dispklay nur minimal dunkler und bleibt kalt.
 */

#define TFT_PIN_CS   10 // Arduino-Pin an Display CS   
#define TFT_PIN_DC   9  // Arduino-Pin an 
#define TFT_PIN_RST  8  // Arduino Reset-Pin

#include <SPI.h>             // SPI für die Kommunikation
#include <Adafruit_GFX.h>    // Adafruit Grafik-Bibliothek wird benötigt
#include <Adafruit_ST7735.h> // Adafruit ST7735-Bibliothek wird benötigt

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_PIN_CS, TFT_PIN_DC, TFT_PIN_RST);  // ST7735-Bibliothek Setup

void setup(void) {

  /***
   * ST7735-Chip initialisieren (INITR_BLACKTAB / INITR_REDTAB / INITR_GREENTAB)
   * Muss bei AZ-Delivery 1.77'' 160x128px RGB TFT INITR_GREENTAB sein ansonsten Pixelfehler rechts und unten.
   * Hinweis: https://github.com/adafruit/Adafruit-ST7735-Library/blob/master/examples/soft_spitftbitmap/soft_spitftbitmap.ino#L52
   * Zeile 52-65  
   ***/
   
  tft.initR(INITR_GREENTAB);   
  
}

void loop() {

  /***  
   * Die tft Funktionen kommen aus der Adafruit Grafik-Bibliothek (Adafruit_GFX) die möglichen Funktionen finden sich unter:
   * https://learn.adafruit.com/adafruit-gfx-graphics-library?view=all
   * bzw.
   * https://cdn-learn.adafruit.com/downloads/pdf/adafruit-gfx-graphics-library.pdf
   ***/

  // fillScreen(farbe);
  tft.fillScreen(ST7735_BLACK);

  // drawRect(pos_links,pos_oben,breite,hoehe,farbe);
  tft.drawRect(0,0,128,160,ST7735_BLUE); 
  
  delay(5000); 
}

