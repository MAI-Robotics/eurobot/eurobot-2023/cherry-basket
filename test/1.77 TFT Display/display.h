#include <Adafruit_GFX.h>       //basic display libraries
#include <Adafruit_ST7735.h>    //basic display libraries
#include <SPI.h>                //spi for communication

//display

/* Pinning
 *  Display (Pin) - Arduino Nano
 *  GND  (1) - GND  
 *  VCC  (2) - 5V
 *  SCK  (3) - D13
 *  SDA  (4) - D11
 *  RES  (5) - D8
 *  RS   (6) - D9
 *  CS   (7) - D10
 *  LEDA (8) - 3.3V (or 5V for brighter Backlight)
 */

#define TFT_CS    10            
#define TFT_RST   8  
#define TFT_DC    9 

#define TFT_SCLK 13   
#define TFT_MOSI 11   

int displayCounter=0;           //counter for point count

//Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);

void setCursorWhite() {
    tft.setCursor(20,50); //werte noch anpassen!!
    tft.setTextSize(8);   //werte noch anpassen!!
    tft.setTextColor(ST7735_WHITE);
}

void setCursorBlack() {
    tft.setCursor(20,50); //werte noch anpassen!!
    tft.setTextSize(8);   //werte noch anpassen!!
    tft.setTextColor(ST7735_BLACK);
}

void fix100() { //fix when increasing to the count of 100 (check for 99+ count, then setting it to -100)
    if (displayCounter >= 99) {
        setCursorBlack();
        tft.print(displayCounter);
        displayCounter -= 100;  //resetting counter, only works when incrementing in 1-steps
    }
}

void initialize() {
    tft.initR(INITR_GREENTAB);   //starten des displays
    tft.fillScreen(ST7735_BLACK);
    setCursorWhite();
    fix100();
}

void clearScreen() {
    fix100();
    setCursorBlack();
    tft.print(displayCounter);
    setCursorWhite();
}

void setCount(int setDisplay) {
    displayCounter = setDisplay;
    initialize();
    tft.print(displayCounter);
}

void increment(int incrementDisplay=1) {
    clearScreen();
    displayCounter += incrementDisplay;
    tft.print(displayCounter);
}

void decrement(int decrementDisplay=1) {
    clearScreen();
    displayCounter -= decrementDisplay;
    tft.print(displayCounter);
}
