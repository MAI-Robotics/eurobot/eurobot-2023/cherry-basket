# 1.8" TFT Display by Adafruit
- Wiring: ([Link](https://1drv.ms/u/s!AjWxvMBValH5gfc85Xjwx5qbR7X84w?e=fORtBa))
Test folder for the Adafruit 1.8" TFT Display to be used in the basket-counter.
![Wiring](./wiring.png)