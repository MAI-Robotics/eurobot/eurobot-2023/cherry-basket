#include <Adafruit_GFX.h>    
#include <Adafruit_ST7735.h> 
#include <SPI.h>
/*
    Wiring
*/
//GND (1) - GND * VCC (2) - 5V * SCK (3) - D13 * SDA (4) - D11 * RES (5) - D8 * RS (6) - D9 * CS (7) - D10 * LEDA (8) - 3.3V
#define TFT_CS    10 //CS
#define TFT_RST   8  //RES
#define TFT_DC    9  //RS

#define TFT_SCLK 13  //SCK 
#define TFT_MOSI 11  //SDA

int displayCounter=0;

//Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);

void initialize() {
    tft.fillScreen(ST7735_BLACK);
    tft.setCursor(20,50); //werte noch anpassen!!
    tft.setTextSize(8);   //werte noch anpassen!!
    tft.setTextColor(ST7735_WHITE);
}

void setCount(int setDisplay) {
    displayCounter = setDisplay;
    initialize();
    tft.print(displayCounter);
}

void increment(int incrementDisplay=1) {
    initialize();
    displayCounter += incrementDisplay;
    tft.print(displayCounter);
}

void decrement(int decrementDisplay=1) {
    initialize();
    displayCounter -= decrementDisplay;
    tft.print(displayCounter);
}
