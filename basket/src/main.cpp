#include <Arduino.h>
#include "display.h"
#include "loadcell.h"
#include "pins.h"
#include "logic.h"

void setup() {
    Serial.begin(9600);

    pinMode(TARE_SW, INPUT_PULLUP);
    pinMode(SET_WEIGHT_SW, INPUT_PULLUP);
    pinMode(DEBUG_LED, OUTPUT);

    setupScale();           //erstellt das Waagen objekt und tared
    initialize();           //richtet das display ein, hintergrundfarbe, farbe der ziffern
    setCount(countBalls()); //setzt das display einmalig auf die anzahl der bälle, am anfang 0
}

void loop() {
    Serial.print(countBalls());   //führt countBalls() aus und gibt die Anzahl im Serial Monitor aus
    Serial.print("  ");
    Serial.print(String(ballWeight));
    Serial.print("  ");
    Serial.println(String(readWeight()));

    // for(int i = 0; i < 10; i++) {
    //     setCursorBlackBW();
    //     tft.print(i-1);
    //     setCursorWhiteBW();
    //     tft.print(i);
    //     delay(2000);
    // }

    if(ballWeight != ballWeightOld) {
        setCursorBlackBW();
        tft.print(ballWeightOld);
        ballWeightOld = ballWeight;
        setCursorWhiteBW();
        tft.print(ballWeight);
    }

    if(ballCount != ballCountOld) { //wenn änderung der bälle ...
        setCount(countBalls());     //... wird anzahl auf display geschrieben
    }

    if(digitalRead(TARE_SW) == LOW) {   //tare-sw triggered
        tareScale();
        blinkSlow(1);
    }

    if(digitalRead(SET_WEIGHT_SW) == LOW) { // set-weight-sw triggered
        setWeight(10);
        blinkFast(2);
    }

    delay(25);                          //delay dass es nicht zu schnell wird
}