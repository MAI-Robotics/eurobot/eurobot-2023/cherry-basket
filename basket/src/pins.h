#ifndef PINS_H
#define PINS_H

// display
#define TFT_CS    10        //CS
#define TFT_RST   8         //RES
#define TFT_DC    9         //RS
#define TFT_SCLK 13         //SCK 
#define TFT_MOSI 11         //SDA

// hx711
#define LOADCELL_DOUT_PIN 4 //DT
#define LOADCELL_SCK_PIN 5  //SCK

// taskbar
#define TARE_SW 2
#define SET_WEIGHT_SW 3
#define DEBUG_LED 13

#endif