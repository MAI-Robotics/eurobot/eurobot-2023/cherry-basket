#ifndef LOADCELL_H
#define LOADCELL_H

#include "HX711.h"
#include "pins.h"
#include "logic.h"
#include "display.h"


/*
    Variables
*/
float ballWeight = 1.825; //measured in grams
const float scaleParam =  1985.58;  //calibration
int ballCount;
int ballCountOld=0;

void setCount(int setDisplay);
extern int countBalls();


HX711 scale;
extern Adafruit_ST7735 tft;

void tareScale() {
    tft.drawRect(120, 0, 10, 10, ST7735_BLUE);
    scale.set_scale(scaleParam);
    scale.tare();               // reset the scale to 0
    tft.drawRect(120, 0, 10, 10, ST7735_BLACK);
    setCount(countBalls());
}

//setup scale
void setupScale() {
    scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
    Serial.println("Taring...");
    Serial.println("Please remove all unwanted elements from load cell.");
    delay(500);
    tareScale();
}

//read weight
float readWeight() {
    return scale.get_units();
}

int countBalls() {
    ballCountOld = ballCount;
    ballCount = (int)(readWeight()/ballWeight+0.5);
    if(ballCount <= 0) {    // keep ball count at min of 0
        ballCount = 0;
    } 
    ballCount %= 100;       // keep ball count between 0 and 99
    return ballCount;
}

/**
 * @description: calibrates the scale if given amount of balls inside the basket
 * 
 * @param balls: amount of balls inside basket
 */
void setWeight(int balls) {
    ballWeight = readWeight() / float(balls);   // 10 balls get divided to one
}

#endif