#ifndef LOGIC_H
#define LOGIC_H
#include "pins.h"
#include <Arduino.h>

void blinkSlow(int times) {
    for(int i = 0; i < times; i++) {
        digitalWrite(DEBUG_LED, HIGH);
        delay(1000);
        digitalWrite(DEBUG_LED, LOW);
        delay(1000);
    }
}

void blinkFast(int times) {
    for(int i = 0; i < times; i++) {
        digitalWrite(DEBUG_LED, HIGH);
        delay(200);
        digitalWrite(DEBUG_LED, LOW);
        delay(200);
    }
}


#endif