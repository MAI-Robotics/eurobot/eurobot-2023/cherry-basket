#ifndef DISPLAY_H
#define DISPLAY_H

#include <Adafruit_GFX.h>       //basic display libraries
#include <Adafruit_ST7735.h>    //basic display libraries
#include <SPI.h>                //spi for communication
#include "pins.h"               //Wiring
#include "logic.h"
#include "loadcell.h"

/* Pinning
 *  Display (Pin) - Arduino Nano
 *  GND  (1) - GND  
 *  VCC  (2) - 5V
 *  SCK  (3) - D13
 *  SDA  (4) - D11
 *  RES  (5) - D8
 *  RS   (6) - D9
 *  CS   (7) - D10
 *  LEDA (8) - 3.3V (or 5V for brighter Backlight)
 */

float ballWeightOld=0;
int displayCounter=0;           //counter for point count

//Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);

void setCursorWhite() {
    tft.setCursor(20,50); //werte noch anpassen!!
    tft.setTextSize(8);   //werte noch anpassen!!
    tft.setTextColor(ST7735_WHITE);
}

void setCursorBlack() {
    tft.setCursor(20,50); //werte noch anpassen!!
    tft.setTextSize(8);   //werte noch anpassen!!
    tft.setTextColor(ST7735_BLACK);
}

void setCursorWhiteBW() {
    tft.setCursor(10,130); //werte noch anpassen!!
    tft.setTextSize(3);   //werte noch anpassen!!
    tft.setTextColor(ST7735_WHITE);
}

void setCursorBlackBW() {
    tft.setCursor(10,130); //werte noch anpassen!!
    tft.setTextSize(3);   //werte noch anpassen!!
    tft.setTextColor(ST7735_BLACK);
}

void fix100() { //fix when increasing to the count of 100 (check for 99+ count, then setting it to -100)
    if (displayCounter >= 99) {
        setCursorBlack();
        tft.print(displayCounter);
        displayCounter -= 100;  //resetting counter, only works when incrementing in 1-steps
    }
}

void initialize() {
    tft.initR(INITR_GREENTAB);   //starten des displays
    tft.setRotation(2);
    tft.fillScreen(ST7735_BLACK);
    setCursorWhite();
    fix100();
}

void clearScreen() {
    fix100();
    setCursorBlack();
    tft.print(displayCounter);
    setCursorWhite();
}

void clearScreenBW() {
    // fix100();
    setCursorBlackBW();
    tft.print(ballWeightOld);
    setCursorWhiteBW();
}

void setCount(int setDisplay) {
    clearScreen();
    displayCounter = setDisplay;
    //initialize();
    tft.print(displayCounter);
}

void printBallWeight() {
    clearScreenBW();
    ballWeightOld = ballWeight;
    tft.print(ballWeight);
}

void increment(int incrementDisplay=1) {
    clearScreen();
    displayCounter += incrementDisplay;
    tft.print(displayCounter);
}

void decrement(int decrementDisplay=1) {
    clearScreen();
    displayCounter -= decrementDisplay;
    tft.print(displayCounter);
}


#endif